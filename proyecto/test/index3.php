<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.22/datatables.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.22/datatables.min.js"></script>

<script>
    var monthFilter;
    var table;
    $(document).ready(function () {
        $('#example').DataTable();
        $('#example2').DataTable();
    });
</script>

<html>
<form >
    <select name="proyectos" id="proyectos" class="proyectos">
        <option value="proyecto1">Proyecto 1</option>
        <!-- Esto se rellena con PHP -->
    </select>
    <input type="submit" value="Submit">
</form>

<body>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Código proyecto</th>
                <th>Presupuesto</th>
                <th>Egresos</th>
                <th>Desviación monto</th>
                <th>Desviación porcentual</th>
            </tr>
        </thead>
        <tbody>

<?php
header("Access-Control-Allow-Origin: *");

error_reporting(E_ERROR); 



/*EDIT HERE***************************************************************************************************************************/
$hostname = 'localhost'; // MySQL host, this is probably right, but check with your webhost to be sure
$username = 'cin44043'; // Your MySQL user name
$password = 'ajCmdYxpoh8gI?4'; //Your MySQL password
$database = 'cin44043_mdl_core'; //The name of the database holding your table
$table1 = 'ins_excel'; // The name of your table
$table2 = 'ins_presupuesto'; // The name of your table
$table3 = 'ins_categorias'; // The name of your table

/***************************************************************************************************************************************/

/*DO NOT EDIT BELOW HERE UNLESS YOU KNOW WHAT YOU ARE DOING!****************************************************************************/

//llamada excel
try {
    $conn = new PDO("mysql:host=$hostname; dbname=$database;charset=utf8", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT 
	 c.cod_proyecto
	,c.egresos
	,p.c_monto_ppto AS presupuesto
	,c.egresos - p.c_monto_ppto AS desviacion_montos
	,100 * (c.egresos - p.c_monto_ppto) / p.c_monto_ppto AS desviacion 
FROM ins_presupuesto p
INNER JOIN (
	SELECT 
		 ie.c_proyecto AS cod_proyecto
		,SUM(ie.c_10)  AS egresos 
	FROM ins_excel ie 
	GROUP BY 
		ie.c_proyecto
) c ON (p.id = c.cod_proyecto)");

    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();
    //echo $result["c_1"];
    
		  foreach( $result as $row ) {
			echo "<tr>";
			echo "<td>". $row["cod_proyecto"]. "</td> ";
			echo "<td>". $row["egresos"]. "</td> ";
			echo "<td>". $row["presupuesto"]. "</td> ";
			echo "<td>". $row["desviacion_montos"]. "</td> ";
			echo "<td>". $row["desviacion"]. "</td> ";
			echo "<//tr>";
		  }



}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;


?>


        </tbody>
        <tfoot>
            <tr>
                <th>Código proyecto</th>
                <th>Presupuesto</th>
                <th>Egresos</th>
                <th>Desviación monto</th>
                <th>Desviación porcentual</th>
            </tr>
        </tfoot>
    </table>




    <table id="example2" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Año</th>
                <th>Mes</th>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Ingreso</th>
                <th>Egreso</th>
                <th>Resultado</th>
            </tr>
        </thead>
        <tbody>

<?php
header("Access-Control-Allow-Origin: *");

error_reporting(E_ERROR); 

/*EDIT HERE***************************************************************************************************************************/
$hostname = 'localhost'; // MySQL host, this is probably right, but check with your webhost to be sure
$username = 'cin44043'; // Your MySQL user name
$password = 'ajCmdYxpoh8gI?4'; //Your MySQL password
$database = 'cin44043_mdl_core'; //The name of the database holding your table
$table1 = 'ins_excel'; // The name of your table
$table2 = 'ins_presupuesto'; // The name of your table
$table3 = 'ins_categorias'; // The name of your table

/***************************************************************************************************************************************/

/*DO NOT EDIT BELOW HERE UNLESS YOU KNOW WHAT YOU ARE DOING!****************************************************************************/

//llamada excel
try {
    $conn = new PDO("mysql:host=$hostname; dbname=$database;charset=utf8", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT 
	 c_proyecto
	,c_8
	,c_9
	,c_categoria
	,c_subcategoria
	,SUM(c_11) AS c_11
	,SUM(c_10) AS c_10
	,SUM(c_12) AS c_12 
FROM $table1 
GROUP BY 
	 c_proyecto
	,c_8
	,c_9
	,c_categoria
	,c_subcategoria
;");

    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();
    //echo $result["c_1"];
    
		  foreach( $result as $row ) {
			echo "<tr>";
			echo "<td>". $row["c_8"]. "</td> ";
			echo "<td>". $row["c_9"]. "</td> ";
			echo "<td>". $row["c_categoria"]. "</td> ";
			echo "<td>". $row["c_subcategoria"]. "</td> ";
			echo "<td>". $row["c_11"]. "</td> ";
			echo "<td>". $row["c_10"]. "</td> ";
			echo "<td>". $row["c_12"]. "</td> ";
			echo "<//tr>";
		  }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;


?>


        </tbody>
        <tfoot>
            <tr>
                <th>Año</th>
                <th>Mes</th>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Ingreso</th>
                <th>Egreso</th>
                <th>Resultado</th>
            </tr>
        </tfoot>
    </table>
</body>

</html>