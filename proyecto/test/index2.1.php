<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.22/datatables.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.22/datatables.min.js"></script>

<script>
    var monthFilter;
    var table;
    $(document).ready(function () {
        table = $('#example').DataTable();
        $("select.months").change(function () {
            monthFilter = $(this).children("option:selected").val()
            console.log(monthFilter);
            console.log(table.getFilters())
        });
    });
</script>

<html>
<form>
    <select name="proyectos" id="proyectos" class="proyectos">
        <option value="proyecto1">Proyecto 1</option>
        <!-- Esto se rellena con PHP -->
    </select>
    <input type="submit" value="Submit">
</form>

<body>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Año</th>
                <th>Mes</th>
                <th>Ingreso</th>
                <th>Egreso</th>
                <th>Resultado</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>C1</td>
                <td>SC1</td>
                <td>2020</td>
                <td>Enero</td>
                <td>100</td>
                <td>101</td>
                <td>-1</td>
            </tr>
            <tr>
                <td>C1</td>
                <td>SC1</td>
                <td>2020</td>
                <td>Enero</td>
                <td>100</td>
                <td>101</td>
                <td>-1</td>
            </tr>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Año</th>
                <th>Mes</th>
                <th>Ingreso</th>
                <th>Egreso</th>
                <th>Resultado</th>
            </tr>
        </tfoot>
    </table>
</body>

</html>