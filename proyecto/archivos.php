<?php
  //variable para formatos admitidos
  $formatos = array(".jpg",".jpeg",".png",".doc",".docx",".xls",".xlsx",".ppt",".pptx");
  //variable directorio que contiene la ruta
  $directorio = 'archivos';
  //contador de archivos
  $contadorArchivos = 0;
  //verificar si existe en memoria lo que envía el formulario
  if(isset($_POST['boton'])){
    //guardar el nombre del archivo que se va a Subir
    $nombreArchivo = $_FILES['archivo']['name']; //queda guardado dentro de un arreglo global bidimensional llamado FILE
    $nombreTmpArchivo = $_FILES['archivo']['tmp_name']; //nombre del archivo temporal
    $extension = substr($nombreArchivo, strrpos($nombreArchivo, "."));//obtener la extensión del nombre del archivo se corta la cadena de texto y se extrae a partir del punto

    //ahora validar si la extensión está dentro de los formatos validos
    if(in_array($extension, $formatos)){ //esta propiedad evalúa si está dentro del arreglo formato
      //ahora si entra aquí debo mover el archivo a mi carpeta
      if(move_uploaded_file($nombreTmpArchivo, 'archivos/'.$nombreArchivo)){ //función uploaded mueve el archivo con el nombre temporal a la carpeta archivos con el nombre de archivo
        //echo 'archivo subido exitosamente';
        echo '<script language="javascript">alert("archivo subido exitosamente");</script>';
      }else{
        //echo 'ocurrió un error. Intentelo nuevamente';
          echo '<script language="javascript">alert("ocurrió un error. intentelo más tarde");</script>';
      }
    }else{
        //echo 'Tipo de archivo no permitido';
        echo '<script language="javascript">alert("Tipo de archivo no permitido");</script>';
    }
  }
 ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Archivos - Inspira Mgmt</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/MUSA_panel-table-1.css">
    <link rel="stylesheet" href="assets/css/MUSA_panel-table.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Social-Icons.css">
    <link rel="stylesheet" href="assets/css/untitled.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laptop-code"></i></div>
                    <div class="sidebar-brand-text mx-3"><span>Inspira <br>Management<br></span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="cuentas.html"><i class="fas fa-user"></i><span>Cuentas</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="table.html"><i class="fas fa-table"></i><span>Finanzas</span></a><a class="nav-link" href="table.html"><i class="fas fa-table"></i><span>Operaciones</span></a><a class="nav-link active" href="archivos.html"><i class="fas fa-cloud"></i><span>Archivos</span></a>
                        <a
                            class="nav-link" href="inscripcion.html"><i class="fas fa-address-book"></i><span>Inscripción Empresas</span></a>
                    </li>
                    <li class="nav-item" role="presentation"></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <form class="form-inline d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                            <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                            </div>
                        </form>
                        <ul class="nav navbar-nav flex-nowrap ml-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow" role="presentation">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class="d-none d-lg-inline mr-2 text-gray-600 small">Usuario 1</span><img class="border rounded-circle img-profile" src="assets/img/avatars/avatar1.jpeg"></a>
                                    <div
                                        class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Profile</a><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Settings</a>
                                        <a
                                            class="dropdown-item" role="presentation" href="#"><i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Activity log</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a></div>
                    </div>
                    </li>
                    </ul>
            </div>
            </nav>
            
            <div class="container-fluid">
                <div class="d-sm-flex justify-content-between align-items-center mb-4">
                    <h3 class="text-dark mb-0">Archivos</h3>
                </div>
              
                <div class="row">
                    <div class="card shadow mb-4">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h6 class="text-primary font-weight-bold m-0">Subir archivos al sistema</h6>
                        </div>
                    
                    <div class="card-body">
                            <div class="col">
                                <div class="row">
                                    <div class="col-12 col-sm-10 col-md-10 col-lg-12 col-xl-9 offset-lg-1 d-flex flex-wrap padMar mx-auto mb-4">
                                        
                                            <form method="post" action="" enctype="multipart/form-data">
                                              <input type="file" name="archivo" class="btn btn-primary btn-Oscuro" />
                                              <i class="fas fa-file-export fa-2x text-gray-300"></i> 
                                              <input type="submit" class="btn btn-primary btn-Oscuro"  value="Subir" name="boton" >
                                            </form>   
                                    </div>
                                    <div class="col-12 col-sm-10 col-md-10 col-lg-12 col-xl-9 d-flex flex-wrap padMar mx-auto">
                                        <p class="margenesTxts">Archivos permitidos: </p>
                                        <p class="margenesTxts">".jpg",".jpeg",".png",".doc",".docx"</p>
                                        <p class="margenesTxts">".xls",".xlsx",".ppt",".pptx"</p>
                                    </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    </div>
                </div>
            
            
            
            
            
            
            <div class="row">
                <div class="table-responsive table mt-2" id="dataTable-1" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Descargar</th>
                                <th>Nombre de Archivo</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                              <?php
                                if($dir = opendir($directorio)){ // la variable direcctorio que contiene la ruta. la funcion opendir abre la ruta que le asigno. y lo guardo en la variable $dir
                                  while($archivo = readdir($dir)){ //ahora leo lo que haya en la variable dir y lo guardo en mi variable $archivo
                                    if($archivo != '.' && $archivo != '..'){ //para no mostrar el punto y dos puntos del directorio
                                    $contadorArchivos++;
                                    echo '<br>';
                                    echo '<br />';
                                    echo '<tr>';
                                    echo '<td style="width: 20%"><i class="fas fa-file-download fa-2x"></i>&nbsp;';
                                    echo "<a href='descargar.php?archivo=$archivo'>Descargar fichero</a>";
                                    echo "</td>";
                                    echo '<td style="width: 20%"></i>';
                                    echo "<strong>$archivo</strong><br />"; //e imprimo el archivo para mostrarlo
                                    echo '</td>';
                                    echo "";
                                    echo "<td style='width: 20%'><i class='fas fa-file-download fa-2x'></i><a href='borrar_archivo.php?archivo=$archivo'>Eliminar</a><br></td></tr>";
                                    }
                                  }
                                  echo 'Archivos totales: '.$contadorArchivos;
                                }
                              ?>

                            </tr>
                        </tbody>
                        <tfoot>
                            <tr></tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <footer class="bg-white sticky-footer">
        <div class="container my-auto">
            <div class="text-center my-auto copyright"><span>Copyright © Inspira Management 2020</span></div>
        </div>
    </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>
