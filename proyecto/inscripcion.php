<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Table - Inspira Mgmt</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/MUSA_panel-table-1.css">
    <link rel="stylesheet" href="assets/css/MUSA_panel-table.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Social-Icons.css">
    <link rel="stylesheet" href="assets/css/untitled.css">
    <script type="text/javascript">
    function validar(){
      var x = document.getElementById("lista").value
           if(x > 0){
             document.getElementById("cantidad").disabled = false;
           }else{
             document.getElementById("cantidad").disabled = true;
             document.getElementById("nombre1").disabled = true;
           }
    }
    </script>
    <script type="text/javascript">
    function validar2(){
      var z = document.getElementById("cantidad").value
      switch (z) {
        case '1':
        document.getElementById("nombre1").disabled = false;
        document.getElementById("apellidos1").disabled = false;
        document.getElementById("email1").disabled = false;
        document.getElementById("ciudad").disabled = false;
        document.getElementById("educacion1").disabled = false;
        document.getElementById("rut1").disabled = false;
        document.getElementById("f_d_nac1").disabled = false;
          break;
          case '2':
          document.getElementById("nombre2").disabled = false;
          document.getElementById("apellidos2").disabled = false;
          document.getElementById("email2").disabled = false;
          document.getElementById("ciudad2").disabled = false;
          document.getElementById("educacion2").disabled = false;
          document.getElementById("rut2").disabled = false;
          document.getElementById("f_d_nac2").disabled = false;
            break;
            case '3':
            document.getElementById("nombre3").disabled = false;
            document.getElementById("apellidos3").disabled = false;
            document.getElementById("email3").disabled = false;
            document.getElementById("ciudad3").disabled = false;
            document.getElementById("educacion3").disabled = false;
            document.getElementById("rut3").disabled = false;
            document.getElementById("f_d_nac3").disabled = false;
              break;
              case '4':
              document.getElementById("nombre4").disabled = false;
              document.getElementById("apellidos4").disabled = false;
              document.getElementById("email4").disabled = false;
              document.getElementById("ciudad4").disabled = false;
              document.getElementById("educacion4").disabled = false;
              document.getElementById("rut4").disabled = false;
              document.getElementById("f_d_nac4").disabled = false;
                break;
                case '5':
                document.getElementById("nombre5").disabled = false;
                document.getElementById("apellidos5").disabled = false;
                document.getElementById("email5").disabled = false;
                document.getElementById("ciudad5").disabled = false;
                document.getElementById("educacion5").disabled = false;
                document.getElementById("rut5").disabled = false;
                document.getElementById("f_d_nac5").disabled = false;
                  break;
                  case '6':
                  document.getElementById("nombre6").disabled = false;
                  document.getElementById("apellidos6").disabled = false;
                  document.getElementById("email6").disabled = false;
                  document.getElementById("ciudad6").disabled = false;
                  document.getElementById("educacion6").disabled = false;
                  document.getElementById("rut6").disabled = false;
                  document.getElementById("f_d_nac6").disabled = false;
                    break;
                    case '7':
                    document.getElementById("nombre7").disabled = false;
                    document.getElementById("apellidos7").disabled = false;
                    document.getElementById("email7").disabled = false;
                    document.getElementById("ciudad7").disabled = false;
                    document.getElementById("educacion7").disabled = false;
                    document.getElementById("rut7").disabled = false;
                    document.getElementById("f_d_nac7").disabled = false;
                      break;
                      case '8':
                      document.getElementById("nombre8").disabled = false;
                      document.getElementById("apellidos8").disabled = false;
                      document.getElementById("email8").disabled = false;
                      document.getElementById("ciudad8").disabled = false;
                      document.getElementById("educacion8").disabled = false;
                      document.getElementById("rut8").disabled = false;
                      document.getElementById("f_d_nac8").disabled = false;
                        break;
                        case '9':
                        document.getElementById("nombre9").disabled = false;
                        document.getElementById("apellidos9").disabled = false;
                        document.getElementById("email9").disabled = false;
                        document.getElementById("ciudad9").disabled = false;
                        document.getElementById("educacion9").disabled = false;
                        document.getElementById("rut9").disabled = false;
                        document.getElementById("f_d_nac9").disabled = false;
                          break;
                          case '10':
                          document.getElementById("nombre10").disabled = false;
                          document.getElementById("apellidos10").disabled = false;
                          document.getElementById("email10").disabled = false;
                          document.getElementById("ciudad10").disabled = false;
                          document.getElementById("educacion10").disabled = false;
                          document.getElementById("rut10").disabled = false;
                          document.getElementById("f_d_nac10").disabled = false;
                            break;
        default:

      }

/*
           if(z = 1){
               document.getElementById("nombre1").disabled = false;
               document.getElementById("apellidos1").disabled = false;
               document.getElementById("email1").disabled = false;
               document.getElementById("ciudad").disabled = false;
               document.getElementById("educacion1").disabled = false;
               document.getElementById("rut1").disabled = false;
               document.getElementById("f_d_nac1").disabled = false;
           }else{
             document.getElementById("nombre1").disabled = true;
             document.getElementById("apellidos1").disabled = true;
             document.getElementById("email1").disabled = true;
             document.getElementById("ciudad").disabled = true;
             document.getElementById("educacion1").disabled = true;
             document.getElementById("rut1").disabled = true;
             document.getElementById("f_d_nac1").disabled = true;
           }
*/

    }
    </script>
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laptop-code"></i></div>
                    <div class="sidebar-brand-text mx-3"><span>Inspira <br>Management<br></span></div>
                </a>
                <p id="demo"></p>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"></li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="inscripcion.html"><i class="fas fa-address-book"></i><span>Inscripción Empresas</span></a></li>
                    <li class="nav-item" role="presentation"></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="nav navbar-nav flex-nowrap ml-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow" role="presentation">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="www.inspiracapacitacion.cl"><span class="d-none d-lg-inline mr-2 text-gray-600 small">InspiraCapacitación</span><img class="border rounded-circle img-profile" src="assets/img/importada/Logo-Color%20-%20copia.png"></a>
                                    <div
                                        class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Profile</a><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Settings</a>
                                        <a
                                            class="dropdown-item" role="presentation" href="#"><i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Activity log</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a></div>
                    </div>
                    </li>
                    </ul>
            </div>
            </nav>
            <div class="container-fluid">
                <h3 class="text-dark mb-4">Solicitud de Matrícula</h3>
                <form action="valida_inscripcion.php" method="post">
                    <div class="card shadow">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos de la Empresa</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group"><label for="city"><strong>Nombre de la Empresa</strong><br></label><input class="form-control" type="text" placeholder="Empresa SA" name="em_name" required=""></div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="country"><strong>Rut</strong><br></label><input class="form-control" type="text"  name="em_rut" required="" maxlength="10"></div>
                                </div>
                            </div>
                            <div class="form-group"><label for="address"><strong>Dirección</strong><br></label><input class="form-control" type="text" placeholder="Av. Saldivar, 38. Santiago" name="em_address" required=""></div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group"><label for="city"><strong>Persona de Contacto</strong><br></label><input class="form-control" type="text" placeholder="Nombre persona a cargo" name="em_enca" required=""></div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="country"><strong>Teléfono de Contacto</strong><br></label><input class="form-control" type="text" placeholder="Teléfono de contacto" name="enc_tel" required=""></div>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="card shadow">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Llenar la tabla con los colaboradores que deseen matricularse en el curso.</p>
                        <div>
                        <select name="lista" id="lista" onchange="validar();">
                          <option value="0" selected>No</option>
                          <OPTION value="1">Si</OPTION>
                        </select>
                        <select name="cantidad" id="cantidad" disabled="true" onchange="validar2();">
                          <option value="0" selected>0</option>
                          <option value="1">1</option>
                          <OPTION value="2">2</OPTION>
                          <OPTION value="3">3</OPTION>
                          <OPTION value="4">4</OPTION>
                          <OPTION value="5">5</OPTION>
                          <OPTION value="6">6</OPTION>
                          <OPTION value="7">7</OPTION>
                          <OPTION value="8">8</OPTION>
                          <OPTION value="9">9</OPTION>
                          <OPTION value="10">10</OPTION>
                        </select>
                      </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <table class="table my-0" id="dataTable">
                                <thead>
                                    <tr>
                                        <th style="min-width: 150px;">Nombre</th>
                                        <th style="min-width: 150px;">Apellidos</th>
                                        <th style="min-width: 150px;">E-mail</th>
                                        <th style="min-width: 120px;">Ciudad</th>
                                        <th style="min-width: 150px;">Nivel Educacional</th>
                                        <th style="min-width: 120px;">RUT</th>
                                        <th style="min-width: 100px;">F de Nacimiento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre1" name="nombre1" disabled></td>
                                        <td><input type="text" class="form-control" id="apellidos1" name="apellidos1" disabled></td>
                                        <td><input type="email" class="form-control" id="email1"  name="email1" disabled></td>
                                        <td><input type="text" class="form-control" id="ciudad" name="ciudad" disabled></td>
                                        <td><select class="form-control" id="educacion1" name="educacion1" disabled><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut1" name="rut1" maxlength="10" disabled></td>
                                        <td><input class="form-control-sm form-control" id="f_d_nac1" type="date"  name="f_d_nac1" disabled></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre2"  name="nombre2" disabled></td>
                                        <td><input type="text" class="form-control" id="apellidos2" name="apellidos2" disabled></td>
                                        <td><input type="email" class="form-control" id="email2"  name="email2" disabled></td>
                                        <td><input type="text" class="form-control" id="ciudad2" name="ciudad2" disabled></td>
                                        <td><select class="form-control" id="educacion2" name="educacion2" disabled><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut2" name="rut2" maxlength="10" disabled></td>
                                        <td><input class="form-control" type="date" id="f_d_nac2" name="f_d_nac2" disabled></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre3" disabled  name="nombre3"></td>
                                        <td><input type="text" class="form-control" id="apellidos3" disabled name="apellidos3"></td>
                                        <td><input type="email" class="form-control" id="email3" disabled name="email3"></td>
                                        <td><input type="text" class="form-control" id="ciudad3" disabled name="ciudad3"></td>
                                        <td><select class="form-control"  id="educacion3" disabled name="educacion3"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control"  id="rut3" disabled name="rut3" maxlength="10"></td>
                                        <td><input  type="date" id="f_d_nac3" disabled name="f_d_nac3"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre4" disabled  name="nombre4"></td>
                                        <td><input type="text" class="form-control" id="apellidos4" disabled name="apellidos4"></td>
                                        <td><input type="email" class="form-control" id="email4" disabled name="email4"></td>
                                        <td><input type="text" class="form-control" id="ciudad4" disabled name="ciudad4"></td>
                                        <td><select class="form-control" id="educacion4" disabled name="educacion4"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut4" disabled  name="rut4" maxlength="10"></td>
                                        <td><input class="form-control" type="date" id="f_d_nac4" disabled name="f_d_nac4"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre5" disabled  name="nombre5"></td>
                                        <td><input type="text" class="form-control" id="apellidos5" disabled name="apellidos5"></td>
                                        <td><input type="email" class="form-control" id="email5" disabled name="email5"></td>
                                        <td><input type="text" class="form-control" id="ciudad5" disabled  name="ciudad5"></td>
                                        <td><select class="form-control" id="educacion5" disabled  name="educacion5"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut5" disabled name="rut5" maxlength="10" ></td>
                                        <td><input class="form-control" type="date" id="f_d_nac5" disabled name="f_d_nac5"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre6" disabled  name="nombre6"></td>
                                        <td><input type="text" class="form-control" id="apellidos6" disabled name="apellidos6"></td>
                                        <td><input type="email" class="form-control" id="email6" disabled name="email6"></td>
                                        <td><input type="text" class="form-control" id="ciudad6" disabled name="ciudad6"></td>
                                        <td><select class="form-control" id="educacion6" disabled name="educacion6"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut6" disabled name="rut6" maxlength="10"></td>
                                        <td><input class="form-control" type="date" id="f_d_nac6" disabled name="f_d_nac6"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre7" disabled  name="nombre7"></td>
                                        <td><input type="text" class="form-control" id="apellidos7" disabled name="apellidos7"></td>
                                        <td><input type="email" class="form-control" id="email7" disabled name="email7"></td>
                                        <td><input type="text" class="form-control" id="ciudad7" disabled name="ciudad7"></td>
                                        <td><select class="form-control" id="educacion7" disabled name="educacion7"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut7" disabled name="rut7" maxlength="10" ></td>
                                        <td><input class="form-control" type="date" id="f_d_nac7" disabled  name="f_d_nac7"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre8" disabled  name="nombre8"></td>
                                        <td><input type="text" class="form-control" id="apellidos8" disabled name="apellidos8"></td>
                                        <td><input type="email" class="form-control" id="email8" disabled name="email8"></td>
                                        <td><input type="text" class="form-control" id="ciudad8" disabled name="ciudad8"></td>
                                        <td><select class="form-control" id="educacion8" disabled name="educacion8"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut8" disabled name="rut8" maxlength="10" ></td>
                                        <td><input class="form-control" type="date" id="f_d_nac8" disabled  name="f_d_nac8"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre9" disabled  name="nombre9"></td>
                                        <td><input type="text" class="form-control" id="apellidos9" disabled name="apellidos9"></td>
                                        <td><input type="email" class="form-control" id="email9" disabled name="email9"></td>
                                        <td><input type="text" class="form-control" id="ciudad9" disabled name="ciudad9"></td>
                                        <td><select class="form-control" id="educacion9" disabled name="educacion9"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut9" disabled name="rut9" maxlength="10"></td>
                                        <td><input class="form-control" type="date" id="f_d_nac9" disabled name="f_d_nac9"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="nombre10" disabled  name="nombre10"></td>
                                        <td><input type="text" class="form-control" id="apellidos10" disabled name="apellidos10"></td>
                                        <td><input type="email" class="form-control" id="email10" disabled name="email10"></td>
                                        <td><input type="text" class="form-control" id="ciudad10" disabled name="ciudad10"></td>
                                        <td><select class="form-control" id="educacion10" disabled name="educacion10"><optgroup label="Seleccione"><option value="12">Ed Básica</option><option value="13">Ed Media</option><option value="14">Técnica</option><option value="15">Universitaria</option></optgroup></select></td>
                                        <td><input type="text" class="form-control" id="rut10" disabled name="rut10" maxlength="10" ></td>
                                        <td><input class="form-control" type="date" id="f_d_nac10" disabled  name="f_d_nac10"></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr></tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="form-group"><button class="btn btn-primary btn-sm" type="submit">Enviar Solicitud</button></div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright © Inspira Management 2020</span></div>
            </div>
        </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>
