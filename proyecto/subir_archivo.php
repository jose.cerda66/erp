<?php
  //variable para formatos admitidos
  $formatos = array(".jpg",".jpeg");
  //variable directorio que contiene la ruta
  $directorio = 'archivos';
  //contador de archivos
  $contadorArchivos = 0;
  //verificar si existe en memoria lo que envía el formulario
  if(isset($_POST['boton'])){
    //guardar el nombre del archivo que se va a Subir
    $nombreArchivo = $_FILES['archivo']['name']; //queda guardado dentro de un arreglo global bidimensional llamado FILE
    $nombreTmpArchivo = $_FILES['archivo']['tmp_name']; //nombre del archivo temporal
    $extension = substr($nombreArchivo, strrpos($nombreArchivo, "."));//obtener la extensión del nombre del archivo se corta la cadena de texto y se extrae a partir del punto

    //ahora validar si la extensión está dentro de los formatos validos
    if(in_array($extension, $formatos)){ //esta propiedad evalúa si está dentro del arreglo formato
      //ahora si entra aquí debo mover el archivo a mi carpeta
      if(move_uploaded_file($nombreTmpArchivo, 'archivos/'.$nombreArchivo)){ //función uploaded mueve el archivo con el nombre temporal a la carpeta archivos con el nombre de archivo
        //echo 'archivo subido exitosamente';
        echo '<script language="javascript">alert("archivo subido exitosamente");</script>';
      }else{
        //echo 'ocurrió un error. Intentelo nuevamente';
          echo '<script language="javascript">alert("ocurrió un error. intentelo más tarde");</script>';
      }
    }else{
        //echo 'Tipo de archivo no permitido';
        echo '<script language="javascript">alert("Tipo de archivo no permitido");</script>';
    }
  }
 ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
   <HEAD>
      <TITLE>Subir Archivo</TITLE>
   </HEAD>
   <BODY>

        <form method="post" action="" enctype="multipart/form-data">
          <h1>Subir archivo</h1>
          <input type="file" name="archivo" />
          <input type="submit" value="Subir archivo" name="boton" >
        </form>

        <h1>Archivos compartidos</h2>
          <?php
            if($dir = opendir($directorio)){ // la variable direcctorio que contiene la ruta. la funcion opendir abre la ruta que le asigno. y lo guardo en la variable $dir
              while($archivo = readdir($dir)){ //ahora leo lo que haya en la variable dir y lo guardo en mi variable $archivo
                if($archivo != '.' && $archivo != '..'){ //para no mostrar el punto y dos puntos del directorio
                $contadorArchivos++;
                echo "Archivo: <strong>$archivo</strong> <a href='descargar.php?archivo=$archivo'>Descargar fichero</a> <br />"; //e imprimo el archivo para mostrarlo
                echo "";
                }
              }
              echo 'Archivos totales: '.$contadorArchivos;
            }
          ?>
   </BODY>
</HTML>
